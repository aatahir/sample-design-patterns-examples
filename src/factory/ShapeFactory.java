package factory;

public class ShapeFactory {

	public Shape getShape(String shapeType) {

		if (shapeType == null) {
			return null;
		}

		if (shapeType.equals("S")) {

			return new Square(10);

		} else if (shapeType.equals("R")) {
			return new Rectangle(10, 5);

		} else if (shapeType.equals("C")) {
			return new Circle(2);
			
		} else if (shapeType.equalsIgnoreCase("T")) {
			return new Triangle(4, 3);
		}

		return null;

	}
}
