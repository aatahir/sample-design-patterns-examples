package factory;

public class Triangle implements Shape {
	private double base, hight;

	public Triangle(double base, double hight) {
		super();
		this.base = base;
		this.hight = hight;
	}

	@Override
	public double draw() {
		return (base * hight) / 2;
	}

}
