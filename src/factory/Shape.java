package factory;

public interface Shape {
	double draw();
}
